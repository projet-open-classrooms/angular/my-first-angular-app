import { Component, OnInit } from '@angular/core';
import {Poste} from "../post-list-component/post-list-component.component";
import {AppareilService} from "../services/appareil.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-appareil-view',
  templateUrl: './appareil-view.component.html',
  styleUrls: ['./appareil-view.component.scss']
})
export class AppareilViewComponent implements OnInit {

  title = 'My Angular App';
  isAuth = false;

  lastUpdate = new Promise(
    (resolve, rejects) =>{
      const date = new Date();
      setTimeout(
        () =>{
          resolve(date);
        }, 2000
      ) ;
    }
  )

  appareils : any[];
  appareilSubscription: Subscription;
  posts = [
    new Poste('Mon Premier post','Vous allez créer une application simple de type blog.  Cette application va afficher les posts du blog, et chaque post aura un bouton permettant de "love it" ou de "don\'t love it".  Chaque post aura la forme suivante', 'https://mdbootstrap.com/img/Photos/Others/images/49.jpg', 2),
    new Poste('Mon Deuxième post','Je vous conseille d\'utiliser Bootstrap pour cet exercice.  Si vous créez des list-group-item dans un list-group, vous avez les classes list-group-item-success et list-group-item-danger pour colorer les item.".  Chaque post aura la forme suivante', 'https://mdbootstrap.com/img/Photos/Others/images/31.jpg', -5),
    new Poste('Mon Troisième post','Bonus : créez un type pour les post, appelé Post, afin de rendre votre code plus lisible.  Vous pouvez même y intégrer un constructeur qui attribue automatiquement la date !', 'https://mdbootstrap.com/img/Photos/Others/images/52.jpg', 3),
  ]

  constructor(private  appareilService: AppareilService) {
    setTimeout(
      () => {
        this.isAuth = true;
      }, 4000
    );
  }

  ngOnInit(): void {
    this.appareilSubscription = this.appareilService.appareilSubject.subscribe(
      (appareils:any[]) =>{
        this.appareils = appareils;
      }
    );
    this.appareilService.emitAppareilSubject();
  }

  onAllummer(){
    this.appareilService.switchOnAll();
  }

  OnEteindre() {
    if(confirm('Etes-vous sûr de vouloir éteindre tous vos appareils ?')) {
      this.appareilService.switchOffAll();
    } else {
      return null;
    }
  }

  OnSave(){
    this.appareilService.saveAppareilToServer();
  }

  onFetch() {
    this.appareilService.getAppareilFromServer();
  }

}
