import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-post-list-component',
  templateUrl: './post-list-component.component.html',
  styleUrls: ['./post-list-component.component.scss']
})
export class PostListComponentComponent implements OnInit {

  @Input() postes: Array<Poste>;
  constructor() { }

  ngOnInit(): void {
  }
}
export class Poste{
  title: string;
  content: string;
  image: string;
  loveIts: number;
  createdDate: Date;

  constructor(title: string, content: string, image: string, loveIts: number) {
    this.title = title;
    this.content = content;
    this.image = image;
    this.loveIts = loveIts;
    this.createdDate = new Date();
  }


}
