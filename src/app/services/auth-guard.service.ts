import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {AuthSevice} from "./auth.sevice";

@Injectable()
export class AuthGuard implements CanActivate{

  constructor(private authSevice: AuthSevice, private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if(this.authSevice.isAuth){
      return true;
    }else{
      this.router.navigate(['/auth']);
    }
  }

}
