import {User} from "../models/User.model";
import {Subject} from "rxjs";

export class UserService {
  private users : User[] = [
    {
      firstName: 'Alex',
      lastName: 'KOUASSEU',
      email: 'ktacenterprise@gmail.com',
      drinkPreference: 'Vin rouge',
      hobbies:[
        'coder',
        'Jeux Vidéos',
        'Lecture'
      ]
    }
  ];
  userSubject = new Subject<User[]>();

  emitUsers(){
    this.userSubject.next(this.users.slice());
  }

  addUser(user: User){
    this.users.push(user);
    this.emitUsers();
  }
}
