import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from "rxjs/Observable";
import 'rxjs/Rx'
import {Subscription} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  seconde: number;
  counterSubsription: Subscription;
  constructor() {
  }
  ngOnInit(): void {
    const counter = Observable.interval(1000);
    this.counterSubsription = counter.subscribe(
      (value: number) => {
        this.seconde = value;
      }
    );
  }

  ngOnDestroy(): void {
    this.counterSubsription.unsubscribe();
  }
}
